
def convergence(E, h):

    # Convergence rates
    from math import log as ln #(log is a dolfin name too)
    print "Convergence rate:"
    print "h=%10.2E  E %2.6E" % (h[0], E[0])
    for i in range(1, len(E)):
        r = ln(E[i]/E[i-1])/ln(h[i]/h[i-1])
        print "h=%10.2E  E %2.6E r=%.2f" % (h[i], E[i], r)


E = [3.6886292E-05 , 5.1196504E-06 , 6.6993841E-07 , 9.3713805E-08 , 3.9442584E-08]
h = [3.54E-01     ,  1.77E-01     ,  8.84E-02     ,  4.42E-02     ,  2.21E-02]

convergence(E,h)