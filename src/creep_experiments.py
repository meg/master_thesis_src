from sympy.functions.special.delta_functions import Heaviside, DiracDelta
from numpy import exp, linspace, zeros, asarray, savetxt, ones
import matplotlib.pyplot as plt

def maxwell_creep(t, t0, Nt):
    
    E = 10.0
    eta = 2.0
    eps_0 = 0.2
    sigma = zeros(Nt)
    for i in range(Nt):
        sigma[i] = (1./E + 1./eta*(t[i]-t[t0]))*eps_0*Heaviside(t[i]-t[t0])
    return sigma 

def kv_creep(t, t0, Nt):
    E = 10.0
    eta = 2.0
    eps_0 = 0.2
    sigma = zeros(Nt)
    for i in range(Nt):
        sigma[i] = 1./E*(1 - exp(-E/eta*(t[i]-t[t0])))*eps_0*Heaviside(t[i]-t[t0])
    return sigma

def sls_creep(t, t0, Nt):
    E1 = 10.0
    E2 = 10.0
    eta = 2.0
    eps_0 = 0.2
    tau_e = eta/E2
    tau_s = eta*((E1+E2)/(E1*E2))
    print eps_0/E1
    print eps_0*tau_e/(E1*tau_s)
    sigma = zeros(Nt)
    for i in range(Nt):
        sigma[i] = 1./E1*(1 + (tau_e/tau_s - 1)*exp(-(t[i]-t[t0])/tau_s))*eps_0*Heaviside(t[i]-t[t0])
    return sigma

t = linspace(0, 2, 4001)
Nt = len(t)
t0 = int(Nt/6.)

# eps_0 = zeros(Nt)
# eps_0[t0+1:] = 0.2
# savetxt("step_strain.txt", eps_0)
# print "OK"

eps = sls_creep(t, t0, Nt)
savetxt("sls_creep.txt", eps)
# print 0.105/(2./3)
print "OK"

