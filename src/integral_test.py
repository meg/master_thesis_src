""" 
Testing two different methods of approximating an integral in time:
 - Trapezoidal sum
 - Efficient sum based on the trapezoidal sum 

 This script calculates solutions for a given integral and compares them tol
 the exact solution for both methods.
"""

from matplotlib.pyplot import *
from trapeziodal_test import Trap_Tester, Efficient_Trap_Tester
import numpy as np
import math as m
import sys, time

def compute_trapezoidal(n):
  a = 0; b = 1; 
  test = Trap_Tester(a, b, n)
  t = test.t
  dt = test.dt
  sol = np.zeros(n)
  # trapn = 0.5*dt*test.Ds(0)*test.eps(t[-1])
  sol[0] = test.trapezoidal(0)
  for i in range(1,n):
    sol[i] = test.trapezoidal(i) + test.trapn(i) 
  error = test.exact(t) - sol
  E = m.sqrt(dt*sum(error**2))
  return E, t_comp

def compute_efficient(n):
	a = 0; b = 1;
	test = Efficient_Trap_Tester(a, b, n)
	t = test.t
	dt = test.dt 
	sol = np.zeros(n)
	sol[0] = test.efficient_sum(0)
	for i in range(1, n):
		sol[i] = test.efficient_sum(i) + test.efficient_sum_end(i)
	error = test.exact(t) - sol
	E = m.sqrt(dt*sum(error**2))
	return E, t_comp

def compute(n, rule):
  a = 0; b = 1; 
  test = rule(a, b, n)
  t = test.t
  dt = test.dt
  sol = np.zeros(n)
  # trapn = 0.5*dt*test.Ds(0)*test.eps(t[-1])
  t0 = time.time()
  sol[0] = test.sum(0)
  for i in range(1,n):
    sol[i] = test.sum(i) + test.end_term(i) 
  t1 = time.time()
  t_comp = t1 - t0
  error = test.exact(t) - sol
  E = m.sqrt(dt*sum(error**2))
  return E, t_comp

def convergence(compute, rule):
    h = []  # dt
    E = []  # errors
    nlist = [10, 20, 40, 80, 160, 320, 640, 1280]
    N = 8
    for n in nlist:
        h.append(1./n)
        Eval, t = compute(n, rule)
        E.append(Eval)


    print "Finished computing h and E."
    print E

    # Convergence rates
    from math import log as ln #(log is a dolfin name too)
    print "Convergence rate:"
    print "h=%10.2E, E=%12.10E, r=--" % (h[0], E[0]) 
    for i in range(1, len(E)):
        r = ln(E[i]/E[i-1])/ln(h[i]/h[i-1])
        print "h=%10.2E, E=%12.10E, r=%.4f" % (h[i], E[i], r)
    # Return time taken to compute last (longest) sum
    return t

def plot_stuff(rule):
	n = 101
	if rule == "trap":
		E, sol, t, exact = compute_trapezoidal(n)
	elif rule == "efficient":
		E, sol, t, exact = compute_efficient(n)

	plot(t, exact, '-', t, sol, '--')
	legend(['Exact', 'Numerical (%s)' % rule])
	show()


if __name__ == '__main__':
  print "--------- Regular trapezoidal rule -----------"
  t0 = convergence(compute, Trap_Tester)
  # plot_stuff("trap")
  print "----------------------------------------------"
  print "-------- Efficient trapezoidal rule ----------"
  t1 = convergence(compute, Efficient_Trap_Tester)
  # plot_stuff("efficient")
  print "Time taken for regular trapezoidal rule: ", t0
  print "Time taken for efficient trapezoidal rule: ", t1
  print "Speedup: ", float(t0)/(t1)

  h = []
  E_T = []  # errors
  E_E = []
  nlist = [10, 20, 40, 80, 160, 320, 640, 1280]
  N = 8
  for n in nlist:
      h.append(1./n)
      Eval, t = compute(n, Trap_Tester)
      E_T.append(Eval)
      Eval, t = compute(n, Efficient_Trap_Tester)
      E_E.append(Eval)


  print "Finished computing h and E."

  print "Differences (T, E):"

  for i in range(len(E_E)):
      print "h=%6.2g, E_T=%10g, E_E=%10g, diff=%g" % (h[i], E_T[i], E_E[i], E_T[i]-E_E[i]) 