from dolfin import *
import sys

top = 3.0
cordval = int(sys.argv[1])
mesh = Mesh(Cylinder(Point(0.0, 0.0, top), Point(0.0, 0.0, 0.0), 0.6, 20), cordval)
f = File("cylinder_mesh_%d.xml" %cordval)
f << mesh