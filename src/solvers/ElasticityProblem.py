"""
Copyright (C) 2013 Nina Kristine Kylstad

First added:  2013-10-03
"""

from dolfin import *
import sys
import time
import numpy as np
from scipy.interpolate import splrep, splev

class ElasticityProblem(object):
    """
    Superclass for general elasticity problem.
    """
    def __init__(self, degree, dim, dt, T, direct=False, top=None, 
                    makeplot=False):
        """
        A class describing a general elasticity problem.

        :param degree:	The degree of the solution.
        :type degree:	int.
        :param dim:		The dimension of the problem. Can be 1(D), 2(D) or 3(D).
        :type dim:		int.
        :param makeplot: Turn plotting on / off.
        :type makeplot: bool.
        """
        if dim != 2 and dim != 3:
            print "Error! Dimension must be 2 or 3, you entered %s" % str(dim)
            sys.exit(1)

        self.degree = int(degree)
        self.dim = int(dim)
        self.bcs = []
        self.makeplot = makeplot

        self.dt = dt 
        self.Nt = int(round(T/dt))
        self.t = np.linspace(0, T, self.Nt+1)

        self.direct = direct

    def setup(self, Nx, mesh=None, rectangle=False, cord=False, cordval=0):
        """
        Sets up the mesh, the function space, the normal vector, and test and
        trial functions.

        :param Nx:          The mesh resolution
        :type Nx: int.
        :param mesh:        Include for user-specified mesh 
        :type mesh: dolfin::Mesh 
        :param rectangle:   Set to True for a rectangular mesh 
        :type rectangle: bool.
        :param cord:        Set to True for a spinal cord mesh.
        :type cord: bool
        :param cordval:     Determines the resolution of a cord mesh. <100 results
                            in a simplified mesh, >100 in actual SC mesh.
        :type cordval: int.
        """
        self.set_mesh(Nx, mesh=mesh, rectangle=rectangle, cord=cord,
                        cordval=cordval)
        self.V = VectorFunctionSpace(self.mesh, "CG", self.degree)
        self.n = FacetNormal(self.mesh)
        self.u = TrialFunction(self.V)
        self.v = TestFunction(self.V)

    def set_mesh(self, Nx, mesh=None, rectangle=False, cord=False, cordval=0):
        """
        Creates the mesh.
        :param Nx:          The mesh resolution
        :type Nx: int.
        :param mesh:        Include for user-specified mesh 
        :type mesh: dolfin::Mesh 
        :param rectangle:   Set to True for a rectangular mesh 
        :type rectangle: bool.
        :param cord:        Set to True for a spinal cord mesh.
        :type cord: bool
        :param cordval:     Determines the resolution of a cord mesh. <100 results
                            in a simplified mesh, >100 in actual SC mesh.
        :type cordval: int.
        :param Nx
        """
        if mesh is not None:
            self.mesh = mesh 
        else:
            dim = self.dim
            self.mygeom = "square"
            if dim == 2:
                if rectangle:
                    self.top = 3
                    self.mesh = RectangleMesh(0, 0, 1, 3, Nx, self.top*Nx)
                    self.mesh_s = "RectangleMesh(0, 0, 1, 3, Nx, 3*Nx)"
                else:
                    self.top = 1
                    self.mesh = UnitSquareMesh(Nx,Nx)
                    self.mesh_s = "UnitSquareMesh(Nx,Nx)"
            elif dim == 3:
                if rectangle:
                    self.top = 3
                    nx = int(Nx)
                    self.mesh = Mesh("../meshes/box_mesh.xml")
                    self.mygeom = "rectangle"
                    self.mesh_s = "Mesh(\"/home/nina/Dropbox/bitbucket/master_thesis_src/src/meshes/box_mesh.xml\")"
                elif cord:
                    if cordval < 100:
                        self.mygeom = "cord_simple"
                        self.top = 2.99
                        self.mesh = Mesh("../meshes/simplified_spinal_cord_%d.xml"%cordval)
                        self.mesh_s = "Mesh(\"/home/nina/Dropbox/bitbucket/master_thesis_src/src/meshes/simplified_spinal_cord_%d.xml\")"%cordval
                        # self.mesh = Mesh("../meshes/cylinder_mesh_%d.xml" % cordval)
                    else:
                        self.mygeom = "cord"
                        self.top = 3.38
                        self.mesh = Mesh("../meshes/mesh_closedfissure_coarse.xml.gz")
                        self.mesh_s = "Mesh(\"/home/nina/Dropbox/bitbucket/master_thesis_src/src/meshes/mesh_closedfissure_coarse.xml.gz\")"
                        # self.mesh = Mesh("/usit/abel/karenhes/Mesh/mesh_closedfissure_coarse.xml.gz")
                else:
                    self.mygeom = "cube"
                    self.top = 1
                    self.mesh = UnitCubeMesh(Nx,Nx,Nx)
                    self.mesh_s = "UnitCubeMesh(Nx,Nx,Nx)"

    def save_parameters(self, Nx, pval, degree, dim, dt, makeplot, T, cordval,
                        direct, simple, gamma):
        """
        Saves the parameters used in a simulation to file.

        :param Nx: The mesh resolution.
        :type Nx: int.
        :param pval: The constant pressure value (only for use in simple simulation)
        :type pval: float.
        :param degree: The degree of the elements.
        :type degree: int.
        :param dim: The dimension of the problem.
        :type dim: int.
        :param dt: The time step size.
        :type dt: float.
        :param makeplot: Turns plotting on/off
        :type makeplot: bool.
        :param T: The total simulation time.
        :type T: float.
        :param cordval: Cord mesh resolution.
        :type cordval: int.
        :param direct: Decide if direct solver is used or not. True: direct sover is used. False: Krylov solver is used.
        :type direct: bool.
        :param simple: Decide if simple pressure simulation is run.
        :type simple: bool.
        """
        self.params = {}
        self.params['Nx'] = Nx
        self.params['pval'] = pval
        self.params['degree'] = degree
        self.params['dim'] = dim
        self.params['dt'] = dt
        self.params['makeplot'] = makeplot
        self.params['T'] = T
        self.params['cordval'] = cordval
        self.params['direct'] = direct
        self.params['simple'] = simple
        self.params['gamma'] = gamma
        self.params['mesh'] = self.mesh_s

        f = open("%s/params.txt" %self.path, 'w')
        for par in self.params.keys():
            f.write(par +" : " + str(self.params[par]) + "\n")
        f.close()

    def set_save_file(self, path):
        """
        Sets up the file to save solutions in.
        :param path:
        :type path: str.
        """
        self.path = path
        self.u_file_disp = File("%s/u_solution.pvd" % path, "compressed")
        print "Created u_solution.pvd in directory", path

    def _set_parameters(self, val=None, default=0.0, name="parameter"):
        """
        A private function to set parameter value.

        Sets the a parameter to a given or default value.

        :param val:   	The value to give to the parameter.
        :type val:    	float.
        :param default:	The default value for the parameter.
        :type default:	float.
        :param name:	The name of the parameter.
        :type name:		str.

        :returns: 		float -- the parameter value

        >>> _set_parameters(val=10, name="E")
        Setting E to 10
        >>> _set_parameters(default=10, name="E")
        Setting E to 10
        """
        if val is not None:
            param = val
        else:
            param = default
        print "Setting %s to %g" % (name, param)
        return param


    def eps(self, u):
        """
        Calculates the strain tensor for a given displacement u.

        :param u: The displacement.
        :type u: dolfin.Function.
        :returns: The strain tensor.
        """
        return sym(grad(u))


    def set_bc(self, test_conditions=False):
        """
        Sets up the Dirichlet boundary condition.
        The Dirichlet boundary is predetermined in
        the class DirichletBoundary.

        :Todo: Allow the user to have more control of where
            the Dirichlet boundary is to be placed.
        """
        V = self.V
        top = self.top
        geom = self.mygeom
        # self.boundary_parts = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
        self.boundary_parts = FacetFunction("size_t", self.mesh)
        self.boundary_parts.set_all(0)
        self.Gamma1 = self._DirichletTopBoundary(self.dim, top)
        self.Gamma1.mark(self.boundary_parts, 1)
        self.Gamma2 = self._DirichletBottomBoundary(self.dim, top)
        self.Gamma2.mark(self.boundary_parts, 2)
        self.Gamma3 = self._DirichletTopPointBoundary(self.dim, top, geom)
        self.Gamma3.mark(self.boundary_parts, 3)
        self.Gamma4 = self._DirichletBottomPointBoundary(self.dim, top, geom)
        self.Gamma4.mark(self.boundary_parts, 3)

        # self.ds = ds[self.boundary_parts]
        # DirichletBC
        u_top = Constant([0.0]*self.dim)

        if test_conditions:
            bc_top = DirichletBC(V, u_top, self.boundary_parts, 1)
            bc_bot = DirichletBC(V, u_top, self.boundary_parts, 2)
        else:
            bc_top = DirichletBC(V.sub(self.dim-1), 0, self.boundary_parts, 1)
            bc_bot = DirichletBC(V.sub(self.dim-1), 0, self.boundary_parts, 2)
            bc_hold_top = DirichletBC(V, u_top, self.Gamma3, method="pointwise")
            bc_hold_bot = DirichletBC(V, u_top, self.Gamma4, method="pointwise")
            self.bcs.append(bc_hold_top)
            self.bcs.append(bc_hold_bot)

        self.bcs.append(bc_top)
        self.bcs.append(bc_bot)


    def g(self):
        """ 
        Getter function for the Neumann boundary term.

        :returns: The boundary term.
        """
        return self._g

    def set_g(self, g):
        """
        Setter function for the Neumann boundary term. The parameter **g**
        should be on a form such that it can be multiplied (dot product) with
        the mesh FacetNormal (**n**). Examples of types that can be used are
        listed below, but an Expression is the preferable type.

        :param g: The boundary term.
        :type g: dolfin::Expression, dolfin::Constant, float.
        """
        self._g = g

    def update_g(self, val, pressure=False, time=False, simple=False):
        """
        Assuming the Neumann boundary term is an Expression, this function 
        updates the Expression's pressure- or time-values.

        :param val: The value to be used.
        :type val: float.
        :param pressure: Variable to decide if the pressure value should be 
        updated.
        :type pressure: bool.
        :param time: Varible to decide if the time variable should be updated.
        :type time: bool.
        """
        if pressure:
            self._g.p = val
        elif time:
            if simple:
                self._g.t = val
            else:
                self._g.set_time(val)
        else:
            print "Please choose either pressure=True or time=True."
    
    def f(self):
        """ 
        Getter function for the source term.

        :returns: The source term.
        """
        return self._f

    def set_f(self, f):
        """
        Setter function for the source term. The parameter **f**
        should be on a form such that it can be multiplied (dot product) with
        the TestFunction **v**. Examples of types that can be used are
        listed below.

        :param g: The boundary term.
        :type g: dolfin::Expression, dolfin::Constant.
        """
        self._f = f

    def update_f(self, val, pressure=False, time=False):
        """
        Assuming the source term is an Expression, this function 
        updates the Expression's pressure- or time-values.

        :param val: The value to be used.
        :type val: float.
        :param pressure: Variable to decide if the pressure value should be 
        updated.
        :type pressure: bool.
        :param time: Varible to decide if the time variable should be updated.
        :type time: bool.
        """
        if pressure:
            self._f.p = val
        elif time:
            self._f.t = val
        else:
            print "Please choose either pressure=True or time=True."

    def set_no_rotation(self, gamma):
        """
        Sets up the no-rotation conditions. 

        :param gamma: The stabilization factor.
        :type gamma: float.
        """
        dim, geom = self.dim, self.mygeom
        if dim == 2:
            self.theta_proj = Expression(("0.0", "0.0"))
        elif dim == 3:
            if geom == "cube":
                self.theta_proj = Expression(("-(0.5 + x[1])/sqrt((0.5+x[0])*(0.5+x[0]) + (0.5+x[1])*(0.5+x[1]))","(0.5+x[0])/sqrt((0.5+x[0])*(0.5+x[0]) + (0.5+x[1])*(0.5+x[1]))", "0"))
            elif geom == "cord_simple":
                self.theta_proj = Expression(("-(0.0 + x[1])/sqrt((0.0+x[0])*(0.0+x[0]) + (0.0+x[1])*(0.0+x[1]))","(0.0+x[0])/sqrt((0.0+x[0])*(0.0+x[0]) + (0.0+x[1])*(0.0+x[1]))", "0"))
            else:
                self.theta_proj = Expression(("-(0.7 + x[1])/sqrt((1.5+x[0])*(1.5+x[0]) + (0.7+x[1])*(0.7+x[1]))","(1.5+x[0])/sqrt((1.5+x[0])*(1.5+x[0]) + (0.7+x[1])*(0.7+x[1]))", "0"))
        
        self.no_rotation = Constant([0.0]*self.dim)
        self.h = self.mesh.hmax()
        self.gamma = gamma

    def solver(self, i, a=None, L=None, matrix=False, boundary_parts=None,
    			bcs=None, onlyA=False, onlyB=False):
		# def solver(self):
        """
        A function that solves a generic Finite Element problem
        a(u, v) = L(v).

        :param V: The function space to solve the problem on.
        :type V: dolfin.FunctionSpace
        :param a: The bilinear form (unknown).
        :param L: The linear form (known).
        :param bcs: The Dirichlet boundary conditions.
        :type bcs: list.
        :param boundary_parts: Function marking the boundary
        :type boundary_parts: dolfin.MeshFunction
        :returns: u -- the solution.
        :type u: dolfin.Function
        """
        if boundary_parts is None:
            boundary_parts = self.boundary_parts
        if bcs is None:
            bcs = self.bcs

        # Assemble system:
        if matrix:
            if onlyA:
                self.b = assemble(L, exterior_facet_domains=boundary_parts)
            else:
                self.calculate_vector_b(i)
            A = self.A
            b = self.b
        else:
            A = assemble(a, exterior_facet_domains=boundary_parts)
            b = assemble(L, exterior_facet_domains=boundary_parts)

        # Apply boundary conditions:
        for bc in bcs:
            bc.apply(A, b)
        t0 = time.time()
        # # Solve system:
        u = Function(self.V)
        if self.direct:
            # Use direct (LU) solver
            solve(A, u.vector(), b)
        else:
            # Use iterative (Krylov) solver
            solve(A, u.vector(), b, "gmres", "hypre_amg")
        t1 = time.time()
        print "Time taken to solve: ", t1-t0   
        return u

    def update(self, U, i=0, save_sol=False):
        """
        Function to update a time dependent solution.

        The function calls for a plot of the solution for a given time.

        :param U: The solution to plot.
        :type U: dolfin.Function
        :param t: The time.
        :type t: float.
        """
        if save_sol:
            self.u_file_data = File("%s/u_solution_%05d.xml" % (self.path,i), "compressed")
            self._save(U, self.t[i])

        print "Finished one time loop."

    def _save(self, U, t):
        """
        Save a solution U at time t to file.
        :param U: The solution.
        :type U: dolfin::Function
        :param t: The time.
        :type t: float.
        """
        U.rename("u", "f_0")
        # self.u_file_disp << (U, t)
        self.u_file_data << (U)

    def set_plot(self, makeplot):
        """
        Turn plotting on/off.

        :param makeplot: The state variable.
        :type makeplot: bool.
         """
        self.makeplot = makeplot

    def _plot_solution(self, U, t, save_fig=False):
        """
        Private function that does the plotting of the solution.

        :param U: The solution to plot.
        :type U: dolfin.Function
        :param t: The time.
        :type t: float.
        """
        if self.makeplot:
            plot(U, mode="displacement", interactive=True, title="t=%g" % t)
        else:
            print "Plotting has been disabled."

    def read_pressure_file(self, file_name):
        """
        Reads pressure data from file.
        Copyright Karen H. Stoeverud.

        :param file_name: The file to read from.
        :type file_name: str.
        """
        file_pres = open(file_name)
        lines_pres = file_pres.readlines()
        file_pres.close()
        len_file = len(lines_pres)

        start = 840 #- 170
        stop = 1010
        
        pres = np.zeros(len_file) 
        for i in range(len_file):
          pres[i] = float(lines_pres[i])
        pres_heartbeat = np.array(pres[start:stop])
        pres_heartbeat += 1.8287806 # make sure we have positive pressure values
        pres_heartbeat *= 1.3332 # convert from mmHg to Pa/100
        #print pres_heartbeat
        print len(pres_heartbeat)
        
        tt = np.linspace(0, (stop-start), num=(stop-start))
        tt = tt/200.#sampling frequence
        print len(tt)
        return tt, pres_heartbeat


    class applied_pres(Expression):
        """
        Subclass of dolfin::Expression, creates the pressure wave.
        Copyright Karen H. Stoeverud. Modified by Nina K. Kylstad.
        """
        def __init__(self, v_wave, pres_spline, dim):
            self.a = 0.0
            self.v = v_wave
            self.pres_spline = pres_spline
            self.dim = dim
        def set_time(self, t):
            if t > 0.85:
                tmp = t/0.85
                t = (tmp%1)*0.85
            self.a = t
        def eval(self, value, x):
            v_wave = self.v
            pres_spline = self.pres_spline
            y = x[self.dim-1]  # scale to [0,1]
            value[0] = splev(1./v_wave*(y + v_wave*self.a) % 2.0, pres_spline)

    class applied_pres_sin(Expression):
        """
        Creates simple, sinusoidal pressure wave.
        """
        def __init__(self):
            self.a = 0.0
        def set_time(self, t):
            self.a = t
        def eval(self, value, x):
            y = x[dim-1]
            value[0] = splev(1./v_wave*(y + v_wave*self.a) % 2.0, pres_sin)

    class simple_pres(Expression):
        """
        Creates constant pressure with value sin(2*pi*t)
        """
        def __init__(self):
            self.t = 0.0
        def eval(self, value, x):
            value[0] = np.sin(2*np.pi*self.t)
        def get_val(self):
            return np.sin(2*np.pi*self.t)

    class _DirichletTopBoundary(SubDomain):
        """
        Private class defining the Dirichlet top boundary.
        """

        def __init__(self, dim, top):
            SubDomain.__init__(self)
            self.dim = dim
            self.top = top

        def inside(self, x, on_boundary):
            return on_boundary and x[self.dim - 1] > self.top - DOLFIN_EPS

    class _DirichletTopPointBoundary(SubDomain):
        def __init__(self, dim, top, geom):
            SubDomain.__init__(self)
            self.dim = dim
            self.top = top 
            self.geom = geom

        def inside(self, x, on_boundary):
            tol = 1E-2
            if self.dim == 3:
                if self.geom == "cube":
                    return x[2] > self.top - 1E-10 \
                        and ((x[0]-0.5)**2 + (x[1]-0.5)**2) < DOLFIN_EPS #0.01
                elif self.geom == "cord_simple":
                    return x[2] > self.top - DOLFIN_EPS \
                        and ((x[0])**2 + (x[1])**2) < tol
                        # and -tol < x[0] < tol\
                        # and -tol < x[1] < tol
                        
                elif self.geom == "cord":
                    return x[2] > self.top - DOLFIN_EPS \
                    and ((x[0]+1.5)**2 + (x[1]+0.7)**2) < tol
                    # and -DOLFIN_EPS < x[0] < 0.01\
                    # and -DOLFIN_EPS < x[1] < 0.01
                
                elif self.geom == "rectangle":
                    return x[2] > self.top - DOLFIN_EPS \
                    and ((x[0]-0.5)**2 + (x[1]-1.0)**2) < tol
            
            elif self.dim == 2:
                # This needs to be finished!
                return x[1] > self.top - DOLFIN_EPS \
                and 0.5-tol < x[0] < 0.5+tol

    class _DirichletBottomPointBoundary(SubDomain):
        def __init__(self, dim, top, geom):
            SubDomain.__init__(self)
            self.dim = dim
            self.top = top 
            self.geom = geom

        def inside(self, x, on_boundary):
            tol = 1E-2
            if self.dim == 3:
                if self.geom == "cube":
                    return x[2] < DOLFIN_EPS \
                    and ((x[0]-0.5)**2 + (x[1]-0.5)**2) < DOLFIN_EPS #0.01
                    # and 0.5 - tol < x[1] < 0.5 + tol \
                    # and 0.5 - tol < x[0] < 0.5 + tol
                elif self.geom == "cord_simple":
                    return x[2] < DOLFIN_EPS \
                        and ((x[0])**2 + (x[1])**2) < tol
                        # and -tol < x[0] < tol\
                        # and -tol < x[1] < tol

                elif self.geom == "cord":
                    return x[2] < 0.0232 + DOLFIN_EPS \
                    and ((x[0]+1.5)**2 + (x[1]+0.7)**2) < tol

                elif self.geom == "rectangle":
                    return x[2] < DOLFIN_EPS \
                    and ((x[0]-0.5)**2 + (x[1]-1.0)**2) < tol
                    
                    # and ((x[0])**2 + (x[1])**2) < 0.01 #0.05
            elif self.dim == 2:
                # This needs to be finished!
                return x[1] < DOLFIN_EPS \
                and 0.5-tol < x[0] < 0.5+tol

    class _DirichletBottomBoundary(SubDomain):
        def __init__(self, dim, top):
            SubDomain.__init__(self)
            self.dim = dim
            self.top = top

        def inside(self, x, on_boundary):
            return on_boundary and x[self.dim - 1] < 0.0232 + DOLFIN_EPS


    class _NeumannBoundary(SubDomain):
        """
        Private class defining the Neumann boundary.

        :param dim: The dimension of the problem.
        """

        def __init__(self, dim, top):
            SubDomain.__init__(self)
            self.dim = dim

        def inside(self, x, on_boundary):
            # tol = 1E-14
            return on_boundary and x[self.dim - 1] < DOLFIN_EPS


    class _DirichletTestBoundary(SubDomain):
        """
        Private class defining the Dirichlet boundary.

        :param dim: The dimension of the problem.
        """

        def __init__(self, dim, top, all_over=False):
            SubDomain.__init__(self)
            self.dim = dim
            self.top = top
            self.all = all_over

        def inside(self, x, on_boundary):
            if self.all:
                return on_boundary
            top = self.top
            return on_boundary and (x[self.dim - 1] > top - DOLFIN_EPS \
                                        or x[self.dim - 2] < DOLFIN_EPS)
