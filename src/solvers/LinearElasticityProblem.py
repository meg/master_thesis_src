"""
Copyright (C) 2013 Nina Kristine Kylstad

First added:  2013-10-03
"""

from ElasticityProblem import *
import sys

class LinearElasticityProblem(ElasticityProblem):

	"""
	Subclass of *ElasticityProblem* describing a Linear Elasticity problem.

	:param degree:	The degree of the solution.
	:type degree:	int.
	:param dim:		The dimension of the probmem. Can be 1(D), 2(D) or 3(D).
	:type dim:		int.
	:param makeplot: Turn plotting on / off.
	:type makeplot: bool.	
	"""
	
	def __init__(self, degree, dim, dt, T, direct=False, top=None, 
					makeplot=False):

		"""
		A class describing a linear elasticity problem.

		:param degree:	The degree of the solution.
		:type degree:	int.
		:param dim:		The dimension of the problem. Can be 1(D), 2(D) or 3(D)
		:type dim:		int.
		:param makeplot: Turn plotting on / off.
		:type makeplot: bool.
		"""
		
		ElasticityProblem.__init__(self, degree, dim, dt, T, 
							direct=direct, top=top,makeplot=makeplot)
		print "Problem type: Linear Elasticity, %dD" %self.dim
		# Set up parameters:
		self.lmbda = 0
		self.mu = 0

		print "Setting parameters (default values)"
		self.set_parameters()

	def save_parameters(self, Nx, pval, degree, dim, dt, makeplot, T, cordval,
						direct, simple, gamma):
		ElasticityProblem.save_parameters(self, Nx, pval, degree, dim, dt, 
								makeplot, T, cordval, direct, simple, gamma)
		params = self.params
		params["E"] = self.E
		params["nu"] = self.nu 

	def set_test_bc(self):
		V = self.V
		top = self.top
		geom = self.mygeom
		# self.boundary_parts = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
		self.boundary_parts = FacetFunction("size_t", self.mesh)
		self.boundary_parts.set_all(0)
		self.Gamma1 = self._DirichletTopBoundary(self.dim, top)
		self.Gamma1.mark(self.boundary_parts, 1)

		# self.ds = ds[self.boundary_parts]
		# DirichletBC
		u_top = Constant([0.0]*self.dim)
		bc_top = DirichletBC(V, u_top, self.boundary_parts, 1)
		self.bcs.append(bc_top)

	def set_parameters(self, E=None, nu=None):
		
		"""
		Method for setting the relevant parameters of the problem.

		:param E: The Youngs modulus value. Default E=10.
		:type E: float.
		:param nu: The Poisson ratio. Default nu=0.3.
		:type nu: float. 
		"""

		self.E = ElasticityProblem._set_parameters(self, E, 10.0, "E")
		self.nu = ElasticityProblem._set_parameters(self, nu, 0.3, "nu")

		self.mu = Constant(self.E/(2.0*(1.0 + self.nu)))
		self.lmbda = Constant(self.E*self.nu/((1.0 + self.nu)*(1.0 - 2.0*self.nu)))


	def sigma(self,u, i=None):
		"""
		Method calculating the stress tensor (sigma) for the elasticity problem.

		:param u: The displacement.
		:type u: dolfin.Function
		:param i: The time level (only relevant for time dependent problems, may be useless here.)
		:returns sigma: The stress tensor
		"""
		eps = self.eps
		mu, lmbda = self.mu, self.lmbda
		sigma = 2*mu*eps(u)+ lmbda*tr(eps(u))*Identity(self.dim)
		return sigma

	def form_a(self, i=0):
		u, v = self.u, self.v
		theta_proj = self.theta_proj
		gamma, h = self.gamma, self.h
		n = self.n

		self.a = inner(self.sigma(u), grad(v))*dx 
		if theta_proj is not None:
			self.a = self.a \
				+ gamma/h*inner(u, theta_proj)*inner(v, theta_proj)*(ds(1)+ds(2)) \
				- inner(self.sigma(v)*n, theta_proj)*inner(u, theta_proj)*(ds(1)+ds(2)) \
				- inner(self.sigma(u)*n, theta_proj)*inner(v, theta_proj)*(ds(1)+ds(2))

		# Assemble matrix
		self.A = assemble(self.a, exterior_facet_domains=self.boundary_parts)

	def calculate_vector_b(self, i=0, f=None, g=None):
		gamma, h = self.gamma, self.h
		v, n = self.v, self.n
		no_rotation = self.no_rotation
		theta_proj = self.theta_proj

		if f is None:
			f = self.f()
		if g is None:
			g = self.g()

		self.L = dot(f,v)*dx + dot(g*n,v)*ds
		if theta_proj is not None:
			self.L = self.L\
				+ gamma/h*inner(no_rotation, theta_proj)*inner(v, theta_proj)*ds(1)\
				+ gamma/h*inner(no_rotation, theta_proj)*inner(v, theta_proj)*ds(2)\
				-inner(no_rotation, theta_proj)*inner(self.sigma(v)*n, theta_proj)*ds(1) \
				-inner(no_rotation, theta_proj)*inner(self.sigma(v)*n, theta_proj)*ds(2)

		# Assemble RHS vector
		self.b = assemble(self.L, exterior_facet_domains=self.boundary_parts)

	def test_convergence(self, p):
		mu, lmbda = self.mu, self.lmbda
		if self.dim == 2:
			f = Expression(
						("p*exp(p*x[0])*((1+p*(x[1]-1))*lmbda + mu + 2.0*p*(x[1]-1)*mu)", 
							"p*exp(p*x[0])*(lmbda + (1 + p*(x[1]-1))*mu)"), 
						p=p, lmbda=lmbda, mu=mu)
			sigma_ = Expression((
						("-exp(p*x[0])*((1 + p*(x[1]-1))*lmbda + 2.0*p*(x[1]-1)*mu)", 
							"-exp(p*x[0])*(1 + p*(x[1]-1))*mu"),
						("-exp(p*x[0])*(1 + p*(x[1]-1))*mu", 
							"-exp(p*x[0])*((1 + p*(x[1]-1))*lmbda + 2.0*mu)")), 
						p=p, lmbda=lmbda, mu=mu)
		elif self.dim == 3:
			f = Expression(("p*p*exp(p*x[0])*x[1]*(x[2] - 1)*(lmbda + 2.0*mu)", \
						"p*exp(p*x[0])*(x[2]-1)*(lmbda + mu)", \
						"p*exp(p*x[0])*x[1]*(lmbda + mu)"),
						p=p, lmbda=lmbda, mu=mu)

			sigma_ = Expression((
						("-lmbda - exp(p*x[0])*p*x[1]*(x[2] - 1)*(lmbda + 2.0*mu)", 
							"-exp(p*x[0])*(x[2]-1)*mu", "-exp(p*x[0])*x[1]*mu"),
						("-exp(p*x[0])*(x[2]-1)*mu", 
							"(-1 -exp(p*x[0])*p*x[1]*(x[2] - 1))*lmbda", "-mu"), 
						("-exp(p*x[0])*x[1]*mu", "-mu", 
							"(-1-exp(p*x[0])*p*x[1]*(x[2] - 1))*lmbda - 2.0*mu")),
						p=p, lmbda=lmbda, mu=mu)
		# h = sigma_*n
		return f, sigma_

	def exact_convergence(self, p):
		if self.dim == 2:
			return Expression(("(1-x[1])*exp(p*x[0])","(1-x[1])*exp(p*x[0])"), p=p)
		elif self.dim == 3:
			return Expression(("x[1]*(1-x[2])*exp(p*x[0])","1 - x[2]", "1 - x[2]"), p=p)



