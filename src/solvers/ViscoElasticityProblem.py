# """
# Copyright (C) 2013 Nina Kristine Kylstad

# First added:  2013-10-03
# """
 
#from dolfin import *
from ElasticityProblem import *
from numpy import exp
import sys


class ViscoElasticityProblem(ElasticityProblem):

    def __init__(self, degree, dim, dt, T, direct=False, top=None, 
                    makeplot=False, save_file=None):
        """
        A class describing a linear viscoelasticity problem.

        :param degree:  The degree of the solution.
        :type degree:   int.
        :param dim:     The dimension of the problem. Can be 1(D), 2(D) or 3(D)
        :type dim:      int.
        :param t:       Time array containing time steps at which to solve.
        :type t:        array.
        :param V:       The function space for the solution.
        :type V:        dolfin::FunctionSpace
        :param n:       Normal vector to the mesh.
        :type n:        dolfin::FacetNormal
        :param makeplot: Turn plotting on / off.
        :type makeplot: bool.
        """           
        ElasticityProblem.__init__(self, degree, dim, dt, T, 
                        direct=direct, top=top,makeplot=makeplot)
        print "Problem type: Linear Viscoelasticity, %dD" %self.dim

        self.up = 0
        self.u0 = 0
        self.theta_proj = None
        self.no_rotation = None
        self.I = Identity(self.dim)
        print "Setting parameters (default values)"
        self.set_parameters(0)

    def save_parameters(self, Nx, pval, degree, dim, dt, makeplot, T, cordval,
                        direct, simple, gamma):
        """
        Saves the parameters used in a simulation to file. See 
        ElasticityProblem.save_parameters.
        """
        ElasticityProblem.save_parameters(self, Nx, pval, degree, dim, dt, 
                                makeplot, T, cordval, direct, simple, gamma)
        params = self.params
        params["E1"] = self.E1
        params["E2"] = self.E2 
        params["eta"] = self.eta 

    def _set_test_bc(self, all_over=False):
        """ 
        Set up boundary conditions used when running test. 
        **NOTE**: This function should not be used in ordinary use of this class 
        to run simulations. FOR TESTING PURPOSES ONLY.

        :param mesh:        The mesh.
        :type mesh:         dolfin::Mesh
        :param all_over:    When true, sets the Dirichlet condition on the entire boundary.
        :type all_over:     bool.
        """
        V = self.V
        self.boundary_parts = FacetFunction("size_t", self.mesh)
        self.boundary_parts.set_all(0)
        self.Gamma0 = ElasticityProblem._DirichletTestBoundary(self.dim, 
                                                    1, all_over=all_over)
        self.Gamma0.mark(self.boundary_parts, 1)
        
        # DirichletBC
        self.u_bc = Expression(("exp(-t)*sin(x[0]*(1 - x[1]))" , 
                                "exp(-t)*sin(x[0]*(1 - x[1]))"), 
                                t=float(self.t[0]))
        bc = DirichletBC(V, self.u_bc, self.boundary_parts, 1)
        self.bcs.append(bc)

    def _update_test_bc(self, t):
        """
        Update the t-value of the test boundary condition.
        **NOTE**: This function should not be used in ordinary use of this class
        to run simulations. FOR TESTING PURPOSES ONLY.

        :param t:   The t-value to be used.
        :type t:    float.
        """
        self.u_bc.t = t

    def set_parameters(self, C, E1=None, E2=None, eta=None):
        """
        Set the parameters for the linear viscoelasticity problem (SLS model).

        :param E1:  Stiffness.
        :type E1:   float.
        :param E2:  Stiffness.
        :type E2:   float.
        :param eta: Viscosity.
        :type eta:  float.
        """
        self.E1 = ElasticityProblem._set_parameters(self, E1, 10.0, "E1")
        self.E2 = ElasticityProblem._set_parameters(self, E2, 10.0, "E2")
        if E1 == None:
            CE1 = None
        else:
            CE1 = C*E1
        if E2 == None:
            CE2 = E2
        else:
            CE2 = C*E2
        self.L1 = ElasticityProblem._set_parameters(self, CE1, 10.0, "L1")
        self.L2 = ElasticityProblem._set_parameters(self, CE2, 10.0, "L2")
        self.eta = ElasticityProblem._set_parameters(self, eta, 0.3, "eta")

        self.tau_sig = self.eta*(self.E2 + self.E1)/(self.E2*self.E1)
        self.tau_eps = self.eta/self.E2
        self.tau_L = self.tau_eps

    def parameters(self):
        """
        Get the parameters for the linear viscoelasticity problem (SLS model).

        :returns: E1, E2, eta -- the parameters.
        """
        return self.E1, self.E2, self.tau_eps

    def D(self, t, get_float=False):
        """
        Stress relaxation function.

        :param t:   t-value to calculate stress relaxation function for.
        :type t:    float.
        :returns: The value of the stress relaxation function for the given t.
        """
        E1 = self.E1
        E2 = self.E2
        tau = self.tau_eps
        if get_float:
            return E1 + E2*exp(-t/tau)
        else:
            return Constant(E1 + E2*exp(-t/tau))

    def L(self, t, get_float=False):
        L1, L2 = self.L1, self.L2
        self.tau_L = self.tau_eps
        tau_L = self.tau_L

        if get_float:
            return L1 + L2*exp(-t/tau_L)
        else:
            return Constant(L1 + L2*exp(-t/tau_L))

    def Ds(self, t, get_float=False):
        """
        Derivative of stress relaxation function with respect to 
        pseudo-time variable *s*.

        :param t:   t-value to calculate derivative for.
        :type t:    float.
        :returns: The value of the stress relaxation function derived for the given t.
        """
        E1 = self.E1
        E2 = self.E2
        tau = self.tau_eps
        if get_float:
            return (E2/tau)*exp(-t/tau)
        else:
            return Constant((E2/tau)*exp(-t/tau))

    def Ls(self, t, get_float=False):
        L1 = self.L1
        L2 = self.L2
        tau_L = self.tau_L
        if get_float:
            return (L2/tau_L)*exp(-t/tau_L)
        else:
            return Constant((L2/tau_L)*exp(-t/tau_L))

    def Dsi(self, i):
        t = self.t
        Ds = self.Ds
        s = 0
        for j in range(1, i):
            s += Ds(t[i]-t[j], get_float=True)
        return s

    def Lsi(self, i):
        t = self.t
        Ls = self.Ls
        s = 0
        for j in range(1, i):
            s += Ls(t[i]-t[j], get_float=True)
        return s

    def set_no_rotation(self, gamma):
        """
        Sets up the no-rotation conditions.  
        See ElasticityProblem.set_no_rotation.
        """
        ElasticityProblem.set_no_rotation(self, gamma)
        # self.sigma_v = Expression("D0 - E2 + E2*exp(-t/tau)", D0=self.D(0), 
        #                 E2=self.E2, t=0, tau=self.tau_eps)
        self.sigma_vD = Expression("D0 - dt/2.0*(Ds0 + Dst) - dt*Dsi", dt=self.dt,
            D0=self.D(0), Ds0=0.0, Dst=0.0, Dsi=0.0)
        self.sigma_vL = Expression("L0 - dt/2.0*(Ls0 + Lst) - dt*Lsi", dt=self.dt,
            L0=self.L(0), Ls0=0.0, Lst=0.0, Lsi=0.0)

    def sigma_LHS(self, u, i):
        """
        Calculates the LHS (unknown) part of sigma(u(t_i)).
        """
        dt = self.dt
        eps = self.eps
        D, Ds = self.D, self.Ds 
        L, Ls = self.L, self.Ls
        I = self.I

        lhs = D(0)*eps(u) + L(0)*div(u)*I

        if i > 0:
            lhs = lhs - Constant(0.5*dt)*Ds(0)*eps(u) - Constant(0.5*dt)*Ls(0)*div(u)*I
        return lhs

    def form_a(self, i=0):
        """ 
        Creates the linear form **a(u,v)**.
        """
        dt = self.dt
        eps, sigma_LHS = self.eps, self.sigma_LHS
        D, Ds = self.D, self.Ds
        u, v = self.u, self.v 
        theta_proj = self.theta_proj
        n = self.n
        I = self.I

        self.a = inner(sigma_LHS(u,i), grad(v))*dx 
        if theta_proj is not None:
            h, gamma = self.h, self.gamma
            self.a = self.a \
                + Constant(gamma)/h*inner(u, theta_proj)*inner(v, theta_proj)*(ds(1)+ds(2)) \
                - inner((self.sigma_vD*eps(v))*n, theta_proj)*inner(u, theta_proj)*(ds(1)+ds(2)) \
                - inner((self.sigma_vL*div(v)*I)*n, theta_proj)*inner(u, theta_proj)*(ds(1)+ds(2))\
                - inner(sigma_LHS(u,i)*n, theta_proj)*inner(v, theta_proj)*(ds(1)+ds(2))
            
        # Assemble matrix:
        self.A = assemble(self.a, exterior_facet_domains=self.boundary_parts)
        # return self.a

    def calculate_vector_b(self, i=0, f=None, g=None):
        """
        Calculates the RHS b-vector.

        :param i: The time step to calculate the vector for.
        :type i: int.
        :param f: The source term. Only to be included if it has not been
        initialized in the class already.
        :param g: The Neumann boundary term. Only to be included if it has not
        been initialized in the class already.
        """
        v, n = self.v, self.n
        sigma_RHS = self.sigma_RHS
        bp = self.boundary_parts
        gamma, h = self.gamma, self.h
        Ls = self.Ls 

        if f is None:
            f = self._f
        if g is None:
            g = self._g

        # L(v):
        self.myform = dot(f,v)*dx + dot(-g*n,v)*ds 
                    
        if self.no_rotation is not None:
            if self.theta_proj is not None:
                self.myform += Constant(gamma)/h*inner(self.no_rotation, self.theta_proj)*inner(v, self.theta_proj)*(ds(1)+ds(2)) \
                            - inner(self.no_rotation, self.theta_proj) \
                            *inner((self.sigma_vD*self.eps(v) + self.sigma_vL*div(v)*self.I)*n, self.theta_proj)*(ds(1)+ds(2))
        
        # Putting together b vector:
        self.b = assemble(self.myform, exterior_facet_domains=bp)
        if i > 0:
            self.b += sigma_RHS(i)

    def sigma_RHS(self, i):
        """
        Calculates the RHS (known) part of sigma(u(t_i)).
        """
        u0, v = self.u0, self.v
        eps = self.eps 
        Ds, Ls = self.Ds, self.Ls 
        dt = self.dt 
        up = self.up 
        bp = self.boundary_parts
        tau = self.tau_eps
        n = self.n
        theta_proj, no_rotation = self.theta_proj, self.no_rotation

        if i == 0:
            # Return zero
            print "ERROR"
            sys.exit(1)

        tmp_first = assemble(inner(Constant(0.5*dt)*(Ds(i*dt)*eps(u0) + Ls(i*dt)*div(u0)*self.I), grad(v))*dx, 
                                exterior_facet_domains=bp)
        rhs = tmp_first.copy()
        if theta_proj is not None:
            tmp_first_n = assemble(inner(Constant(-0.5*dt)*(Ds(i*dt)*eps(u0) + Ls(i*dt)*div(u0)*self.I)*n, theta_proj)
                                    *inner(v, theta_proj)*(ds(1)+ds(2)), exterior_facet_domains=bp)
            rhs += tmp_first_n

        if i > 1:
            if i > 2:
                self.prev_sum *= exp(-dt/tau)
                tmp_sum = self.prev_sum.copy()
                tmp_sum *= dt 
                rhs += tmp_sum
                if theta_proj is not None:
                    self.prev_sum_n *= exp(-dt/tau)
                    tmp_sum_n = self.prev_sum_n.copy()
                    tmp_sum_n *= -dt 
                    rhs += tmp_sum_n

            end_term = assemble(inner((Ds(dt)*eps(up) + Ls(dt)*div(up)*self.I), grad(v))*dx, exterior_facet_domains=bp)
            tmp_end = end_term.copy()
            tmp_end *= dt
            rhs += tmp_end
            self.prev_sum += end_term
            if theta_proj is not None:
                end_term_n = assemble(inner((Ds(dt)*eps(up) + Ls(dt)*div(up)*self.I)*n, theta_proj)
                                    *inner(v, theta_proj)*(ds(1)+ds(2)), exterior_facet_domains=bp)
                tmp_end_n = end_term_n.copy()
                tmp_end_n *= -dt
                rhs += tmp_end_n 
                self.prev_sum_n += end_term_n

        return rhs

    def update(self, U, i=0, save_sol=False):
        """
        Updates the problem to the current time step. Saves the current 
        solution to a list so that it can be accessed.

        :param U: The solution to be saved.
        :type U: dolfin::Function.
        :param t:
        """
        v, t = self.v, self.t
        Ds, eps = self.Ds, self.eps
        Ls = self.Ls
        dt = self.dt
        E2, tau = self.E2, self.tau_eps
        theta_proj = self.theta_proj
        n = self.n

        if i == 0:
            self.u0 = U
            self.prev_sum = assemble(inner(self.eps(Function(self.V)), grad(v))*dx)
            if theta_proj is not None:
                self.prev_sum_n = assemble(inner(self.eps(Function(self.V))*n, theta_proj)*inner(v, theta_proj)*(ds(1)+ds(2)))
        self.up = U
        if i < len(t)-1:
            #     self.sigma_v.t = t[i+1]
            self.sigma_vD.Ds0 = Ds(0,get_float=True)
            self.sigma_vD.Dst = Ds((i+1)*dt, get_float=True)
            self.sigma_vD.Dsi = self.Dsi(i+1)
            self.sigma_vL.Ls0 = Ls(0,get_float=True)
            self.sigma_vL.Lst = Ls((i+1)*dt, get_float=True)
            self.sigma_vL.Lsi = self.Lsi(i+1)

        ElasticityProblem.update(self, U, i=i, save_sol=save_sol)
