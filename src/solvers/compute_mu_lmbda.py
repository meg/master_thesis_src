def compute_mu_lmbda(E, nu):
    # mu = Constant(E/(2.0*(1.0 + nu)))
    mu = 0.5*E
    G = mu
    lmbda = 2*G*nu/(1 - 2.0*nu)
    # lmbda = E*self.nu/((1.0 + nu)*(1.0 - 2.0*nu))
    return mu, lmbda
E = 0.84e3
E2 = 2.03E3
nu = 0.479

mu, lmbda = compute_mu_lmbda(E2, nu)
C = lmbda/E
print "C:", C
print "mu: ", mu

print "lambda: ", lmbda
