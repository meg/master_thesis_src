from LinearElasticityProblem import *
from ViscoElasticityProblem import *

# Adjust log level
set_log_level(PROGRESS)

# Turn on optimization
parameters["form_compiler"]["cpp_optimize"] = True 
# parameters["form_compiler"]["representation"] = "uflacs"

ffc_options = {"optimize": True, \
                        "eliminate_zeros": True, \
                        "precompute_basis_const": True, \
                        "precompute_ip_const": True} 


def compute(prob_type, model_params, Nx, pval, degree, dim, dt=0.1,
            makeplot=False, T=0.0, gamma=2.0e3, direct=False, rectangle=False,
            cord=False, cordval=0):
    
    # Set up problem
    if prob_type == 'viscoElast':
        problem = ViscoElasticityProblem(degree, dim, dt, T, direct=direct)
        E1 = model_params['E1']; E2 = model_params['E2']
        eta = model_params['eta']
        problem.set_parameters(E1, E2, eta)
    elif prob_type == 'linElast':
        problem = LinearElasticityProblem(degree, dim, dt, T, direct=direct)
        E = model_params['E']; nu = model_params['nu']
        problem.set_parameters(E=E, nu=0.479)
    else:
        print "Error. Please enter valid problem type."

    # Set up mesh, function space, functions etc.
    problem.setup(Nx, mesh=None, rectangle=rectangle, cord=cord, cordval=cordval)
    
    Nt = problem.Nt
    t = problem.t

    # Set up boundaries, Dirichlet BCs and NO-ROTATION condition.
    problem.set_bc()
    problem.set_no_rotation(gamma)

    # Set up body force
    f = Constant([0.0]*dim)

    # Set up boundary pressure
    p = pval*np.ones(Nt+1)
    gval = Expression("p", p=float(p[0]))

    problem.set_g(gval)
    problem.set_f(f)

    save_umax = 0.0

    # Special case for the first time step for viscoElast. For linElast, 
    # this will just calculate the first time step normally.
    problem.form_a(0)
    U0 = problem.solver(0, matrix=True)
    problem.update(U0, save_sol=save_sol)

    # Set up time loop for computation
    progress = Progress("Time-stepping...")

    if prob_type == 'viscoElast':
        # Update bilinear form to include all terms
        problem.form_a(1)

    for i in range(1, Nt+1):
        # Check progress
        print i, " of ", Nt
        progress.update(i*dt/T)

        # Update parameters
        problem.update_g(float(p[i]), pressure=True)

        # Solve
        U = problem.solver(i, matrix=True)
        tol = DOLFIN_EPS
        c = 240
        if save_umax <= abs(U.vector().array()[c]) - tol :
            print "OK!"
            save_umax = abs(U.vector().array()[c])
            print save_umax
        else:
            print "Found max creep value, u=", save_umax
            return save_umax
        # Update solution, save the solution to file if save_sol is True.
        problem.update(U, i=i, save_sol=save_sol)

    print "gamma/h: ", problem.gamma/problem.h 

    print "Finished simulation."
    return save_umax

def find_param_linElast(tol):
    for E in np.linspace(49, 55, 500):
        model_params_e['E'] = E
        u_max_e = compute('linElast', model_params_e, Nx, pval, degree, dim, dt=dt,
    T=0.5, gamma=2.0e3)
        if abs(u_max_v-u_max_e) < tol:
            return model_params_e['E']

def find_param_viscoElast(tol):
    for E in np.linspace(2.62e5, 2.64e5, 20):
        model_params_v['E1'] = E
        model_params_v['E2'] = E*2.5
        model_params_v['eta'] = E/125.
        u_max_e = compute('viscoElast', model_params_v, Nx, pval, degree, dim, dt=dt,
    T=0.5, gamma=2.0e3)
        if abs(u_max_v-u_max_e) < tol:
            return model_params_v

if __name__ == '__main__':
    Nx = 10
    pval = 2
    degree = 1
    dim = 2
    dt = 0.005
    T = 1.0
    save_sol = False
    simple = True
    prob_type = 'viscoElast'
    model_params_v = {'E1':0.84e3, 'E2':2.1e3, 'eta':6.7}
    model_params_e = {'E':16e3, 'nu':0.479}
    u_max_v = compute('linElast', model_params_e, Nx, pval, degree, dim, dt=dt,
        T=0.5, gamma=2.0e3)

    E = find_param_viscoElast(1e-9)
    if E is not None:
        print "E: ", E
    else:
        print "Did not find parameter in specified range."


