from LinearElasticityProblem import *
from ViscoElasticityProblem import *

# Adjust log level
set_log_level(PROGRESS)

# Turn on optimization
parameters["form_compiler"]["cpp_optimize"] = True 
# parameters["form_compiler"]["representation"] = "uflacs"

# ffc_options = {"optimize": True, \
#                         "eliminate_zeros": True, \
#                         "precompute_basis_const": True, \
#                         "precompute_ip_const": True} 


def compute(prob_type, Nx, pval, degree, dim, pres_file, dt=0.1,
            makeplot=False, T=0.0, rectangle=False, cord=False,
            cordval=None, save_sol=False, direct=False, simple=False, 
            path=None, eigentester=False, gamma=2.0e3):
    
    # Set up problem
    if prob_type == 'viscoElast':
        problem = ViscoElasticityProblem(degree, dim, dt, T, direct=direct)
        # E1 = 26.26E4; E2 = 2.5*E1; eta = E1/125.
        E1 = 0.84E3; E2 = 2.1E3; eta = 1e6
        E1 = E1/100; E2 = E2/100; eta=eta/100
        # E1 = 2.5e3/100; E2 = 2.5E3/100; eta = 6.7/100
        C = 20.0
        problem.set_parameters(C, E1=E1, E2=E2, eta=eta)
    elif prob_type == 'linElast':
        problem = LinearElasticityProblem(degree, dim, dt, T, direct=direct)
        # E1 = 0.84E3; E2 = 2.1E3;
        # E = (E1+E2)/100.
        E = 5e3/100.
        problem.set_parameters(E=E, nu=0.0)
    else:
        print "Error. Please enter valid problem type."

    # Set up mesh, function space, functions etc.
    problem.setup(Nx, mesh=None, rectangle=rectangle, cord=cord, cordval=cordval)

    # Set up path for saving solutions and save all parameter values 
    # used to file.
    if save_sol:
        problem.set_save_file(path)
        problem.save_parameters(Nx, pval, degree, dim, dt, makeplot, T, 
                cordval, direct, simple, gamma)
    
    Nt = problem.Nt
    t = problem.t

    # Set up boundaries, Dirichlet BCs and NO-ROTATION condition.
    problem.set_bc()
    problem.set_no_rotation(gamma)

    # Set up body force
    f = Constant([0.0]*dim)

    # Set up boundary pressure
    if simple:
        p = np.zeros(Nt+1)
        p[:Nt/2.] = pval
        gval = Expression("p", p=float(p[0]))
    else:
        v_wave = 200.0 # cm/s
        tt, pres_heartbeat = problem.read_pressure_file(pres_file)
        shift_val = 0.0
        pres_spline = splrep(tt, pres_heartbeat-shift_val)
        gval = problem.applied_pres(v_wave, pres_spline, dim)

    problem.set_g(gval)
    problem.set_f(f)

    if eigentester:
        from conditionnumber import compute_condition_number
        A = PETScMatrix()
        f = open("gamma_val_res%d_%s.txt" %(cordval, prob_type), 'a')
        for g in [1.0e3, 2.0e3, 5e3, 1e4]:
            problem.gamma = g
            problem.form_a(1)
            assemble(problem.a, tensor=A, exterior_facet_domains=problem.boundary_parts)
            for bc in problem.bcs:
              bc.apply(A)
            kappa = compute_condition_number(A)
            print "Condition number = %g, gamma=%g" %(kappa, problem.gamma)
            f.write("Condition number = %10g, gamma=%10g, dt=%10g\n" %(kappa, problem.gamma,dt))
        f.close()
        sys.exit(9)

    # Special case for the first time step for viscoElast. For linElast, 
    # this will just calculate the first time step normally.
    problem.form_a(0)
    U0 = problem.solver(0, matrix=True)
    problem.update(U0, save_sol=save_sol)

    # Set up time loop for computation
    progress = Progress("Time-stepping...")

    if prob_type == 'viscoElast':
        # Update bilinear form to include all terms
        problem.form_a(1)

    for i in range(1, Nt+1):
        # Check progress
        print i, " of ", Nt
        progress.update(i*dt/T)

        # Update parameters
        if simple:
            problem.update_g(float(p[i]), pressure=True)
        else:
            problem.update_g(t[i], time=True, simple=simple)
        c = 240
        # Solve
        U = problem.solver(i, matrix=True)
        print "Value in point %d: "%c, U.vector().array()[c]
        # Update solution, save the solution to file if save_sol is True.
        problem.update(U, i=i, save_sol=save_sol)

    print "gamma/h: ", problem.gamma/problem.h 

    print "Finished simulation."

if __name__ == '__main__':
    Nx = 10
    pval = 2
    degree = 1
    dim = 2
    dt = 0.005
    T = 1.0
    save_sol = False
    simple = True
    compute('viscoElast', Nx, pval, degree, dim, dt=dt, test=False, pressure=True, 
                makeplot=False, T=T, rectangle=False, cord=False,
                cordval=None, get_function=False, E=None, save_sol=save_sol, 
                direct=False, simple=simple, path=None, eigentester=False)
