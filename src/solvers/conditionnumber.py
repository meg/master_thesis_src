"Module for computing eigenvalues and condition numbers."

__author__ = "Marie E. Rognes (meg@simula.no)"
__copyright__ = "Copyright (C) 2012 Marie Rognes"
__license__  = "Distribute and modify at will"

_all__ = ["compute_largest_eigenvalue", "compute_smallest_eigenvalue",
          "compute_condition_number"]

from dolfin import SLEPcEigenSolver
from dolfin import info

def check_complexity(c):
    assert(abs(c) < 1.e-10), \
        "Complex eigenvalue detected, is matrix symmetric?"

def check_converged(n):
    assert(n > 0), "Solver did not converge ..."

def compute_largest_eigenvalue(A):
    "Compute largest (in magnitude) eigenvalue of A."

    info("Computing largest eigenvalue")

    # Solve for largest eigenvalue
    solver = SLEPcEigenSolver(A)
    solver.parameters["solver"] = "krylov-schur"
    solver.parameters["tolerance"] =  1e-15;
    solver.parameters["spectrum"] = "largest magnitude"
    solver.solve(1)

    # Check solver convergence
    num_converged = solver.get_number_converged()
    check_converged(num_converged)

    # Extract largest eigenvalue and check that complex part is zero
    (r, c) = solver.get_eigenvalue(0)
    check_complexity(c)

    return r

def compute_smallest_eigenvalue(A, n=0):
    """Compute smallest (in modulus) eigenvalue of A. If n is given as
    an upper bound of the dimension of the kernel of A, compute
    smallest (in modulus) eigenvalue of A/(ker(A)."""

    info("Computing smallest eigenvalue")

    zero = 1.e-8
    # Solve for smallest in modulues eigenvalue(s)
    solver = SLEPcEigenSolver(A)
    solver.parameters["spectrum"] =  "target magnitude";
    solver.parameters["spectral_transform"] =  "shift-and-invert";
    solver.parameters["spectral_shift"] = zero;
    solver.solve(n+1)

    # Check solver convergence
    num_converged = solver.get_number_converged()
    info("num_converged = %d" % num_converged)
    check_converged(num_converged)

    if n == 0:
        (r, c) = solver.get_eigenvalue(0)
        info("lamda_0 = %g" % r)
        check_complexity(c)
        return r

    for i in range(num_converged):
        (r, c) = solver.get_eigenvalue(i)
        check_complexity(c)
        info("lamda_%d = %g" % (i, r))
        if abs(r) > 1.e-10:
            return r

    raise Exception, "No non-zero eigenvalues found"

def compute_condition_number(A, n=0):
    """Compute condition number of symmetric matrix A. If n is given
    as an upper bound of the dimension of the kernel of A, compute
    smallest (in modulus) eigenvalue of A/(ker(A)."""

    lamda_max = compute_largest_eigenvalue(A)
    lamda_min = compute_smallest_eigenvalue(A, n)

    kappa = abs(lamda_max)/abs(lamda_min)

    return kappa
