#pylint: disable
from ViscoElasticityProblem import *
from LinearElasticityProblem import *
import numpy as np
from time import localtime, strftime
import os
# Adjust log level
set_log_level(PROGRESS)

# Turn on optimization
parameters["form_compiler"]["cpp_optimize"] = True 
parameters["krylov_solver"]["relative_tolerance"] = 1E-13
# parameters["form_compiler"]["representation"] = "uflacs"
#b = {"representation": "uflacs"}               

ffc_options = {"optimize": True, \
                        "eliminate_zeros": True, \
                        "precompute_basis_const": True, \
                        "precompute_ip_const": True} 

def compute(Nx, dt):   
    degree = 2
    dim = 2
    T = 1.0
    print "dt: ", dt
    
    def make_save_path():
        import re
        t = localtime()
        t_str = strftime("%b%d_%H:%M_", t)

        for a in sys.argv[1:]:
          m = re.match("-([a-z]*)", a)
          if m is not None:
            t_str += m.group(1)

        path = 'results/' + t_str + 'conv'
        os.system("mkdir %s" % path)
        return path
    path = make_save_path()
    # Set up viscoelasticity problem
    viscoElast = ViscoElasticityProblem(degree, dim, dt, T, direct=True)
    viscoElast.setup(Nx)
    # path = viscoElast.make_save_path()
    viscoElast.set_save_file(path)

    t, Nt = viscoElast.t, viscoElast.Nt

    # Setting up boundaries
    viscoElast._set_test_bc(all_over=False)
    # viscoElast.set_bc(mesh, V)

    # viscoElast.set_parameters(1.4E3, 1.4E3, 0.499)
    E0, E1, tau = viscoElast.parameters()

    A = "exp(-t)*(E0 + exp(t*(tau - 1)/tau)*E1 - (E0 + E1)*tau)/(tau - 1)"
    Af = "1./(2*(tau - 1))*exp(-t)*(E0*(tau - 1) + E1*(-exp(t*(tau - 1)/tau) + tau))"

    f = Expression(("%s*(cos(x[0] - x[0]*x[1]) + (x[0]*x[0] + x[0]*(x[1] - 1)\
                        + 2*(x[1] - 1)*(x[1] - 1))*sin(x[0] - x[0]*x[1]))"%Af,
                        "%s*(cos(x[0] - x[0]*x[1]) + (2*x[0]*x[0] + x[0]*(x[1] - 1)\
                        + (x[1] - 1)*(x[1] - 1))*sin(x[0] - x[0]*x[1]))"%Af),
                    t=float(t[0]), E0=E0, E1=E1, tau=tau)
    
    sigma_ = Expression((("-cos(x[0] - x[0]*x[1])*(x[1]-1)*%s"%A,\
                            "-cos(x[0] - x[0]*x[1])*(x[0] + x[1] - 1)*%s/2.0"%A), \
                        ("-cos(x[0] - x[0]*x[1])*(x[0] + x[1] - 1)*%s/2.0"%A, \
                            "-cos(x[0] - x[0]*x[1])*x[0]*%s"%A)), \
                        t=float(t[0]),E0=E0, E1=E1, tau=tau)

    # g = sigma_*n
    viscoElast.set_g(sigma_)
    viscoElast.set_f(f)

    gamma = 2.0e3

    viscoElast.set_no_rotation(gamma)
    # viscoElast.theta_proj=None

    # Special case for the first step 
    viscoElast.form_a(0)
    U0 = viscoElast.solver(0, matrix=True)
    viscoElast.update(U0, save_sol=True)
    viscoElast.form_a(1)
    progress = Progress("Time-stepping...")
    for i in range(1, Nt+1):
            print i, " of ", Nt
            progress.update(i*dt/T)

            # Update parameters
            viscoElast.update_g(float(t[i]), time=True, simple=True)
            viscoElast.update_f(float(t[i]), time=True)
            viscoElast._update_test_bc(float(t[i]))

            U = viscoElast.solver(i, matrix=True)
            viscoElast.update(U, i=i, save_sol=True)

    ue = Expression(("exp(-t)*sin(x[0]*(1 - x[1]))" , "exp(-t)*sin(x[0]*(1 - x[1]))"), t=float(t[-1]))
    ue_file = File("%s/u_exact.pvd" % path)
    print "Created file ue_file in directory ", path
    ue_file << project(ue, viscoElast.V)
    E = errornorm(ue, U, "L2")
    
    print "Finished simulation."
    # viscoElast._saveFile(U, t[-1])
    # u_exact_file = File("%s/u_exact.pvd" %path)
    # u_exact_file << project(ue, V)
    # np.savetxt('testfile.txt', project(ue, V).vector().array())
    # import os
    # os.system("cat >> %s/dt.dat << EOL\n dt = %g\nEOL" %(path, dt))
    # plot(U, mode="displacement", interactive=True, title="Numerical")
    # plot(project(ue,V), mode="displacement", title="exact",interactive=True)
    return E, viscoElast.mesh.hmax()


def convergence(compute, Nx=0, dt=0, savefile=None):
    from math import log as ln #(log is a dolfin name too)
    h = []  # element sizes
    E = []  # errors
    if savefile:
        f = savefile
    if Nx == 0 and dt == 0:
        print "Error! Please specify EITHER Nx or dt."
        sys.exit(1)
    elif dt == 0:
        dt_list = [0.02, 0.01, 0.005]#, 0.0025]#0.00125]
        # dt_list = [0.00125]
        for dt in dt_list:
            h.append(dt)
            Eval, hval = compute(Nx, dt)
            E.append(Eval)
        if savefile:
            f.write("\nh : %g\n" %hval)
    elif Nx == 0:
        nlist = [4, 8, 16, 32, 64]
        for N in nlist:
            Eval, hval = compute(N, dt)
            h.append(hval)
            E.append(Eval)
        if savefile:
            f.write("\ndt : %g\n" %dt)

    print "Finished computing h and E."
    print E

    # Convergence rates
    # print "dt: ", dt
    # print "Convergence rate:"
    if savefile:
        f.write("Convergence rates: \n")
        f.write("h/dt=%10.2E E=%7.7E\n" %(h[0], E[0]))
    for i in range(1, len(E)):
        r = ln(E[i]/E[i-1])/ln(h[i]/h[i-1])
        print "h/dt=%10.2E E=%7.7E r=%.2f" % (h[i], E[i], r)
        if savefile:
            f.write("h/dt=%10.2E E=%7.7E r=%.2f\n" % (h[i], E[i], r))

if __name__ == '__main__':
    import sys, time
    t0 = time.time()
    try:
        f = open(sys.argv[1], 'a')
    except IndexError:
        f = open("convergence_results.txt", 'w')
        for Nx in [4, 8, 16, 32, 64]:#, 64, 132]:
            convergence(compute, Nx=Nx, dt=0, savefile=f)
        # Nx = 0
        # dt = 0
        # for dt in [0.02, 0.01, 0.005]:#, 0.0025, 0.00125]:
            # convergence(compute, Nx=Nx, dt=dt, savefile=f)
        f.close()
        # Nx = 10
        # dt = 0.005
        # compute(Nx, dt)

    # dt = float(sys.argv[1])
    # dt = 0.0021

    # E, h = compute(8, dt)
    # print "E: ", E

    t1 = time.time()
    t = t1 - t0
    hrs = int(t)/3600
    rem = t - hrs*3600
    mins = int(rem)/60
    sec = rem - mins*60
    t_tot = "%g hours, %g minutes, %g seconds." %(hrs, mins, sec)

    print "Total time spent: %s" % t_tot