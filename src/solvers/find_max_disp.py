from dolfin import *
import glob, sys
from numpy import savetxt, asarray

try:
    path = sys.argv[1]
except IndexError:
    path = "/home/nina/Dropbox/bitbucket/master_thesis_src/src/solvers/results/May09_13:22_svc"
# path = "/home/nina/Dropbox/bitbucket/master_thesis/src/results_abel/May08_14:58_scv"
f = open("%s/params.txt" %path)
lines = f.readlines()
params = {}
for line in lines:
    words = line.split(' : ')
    val = words[1].rstrip()
    params[words[0]] = val

print params
f.close()

degree = int(params['degree'])
Nx = int(params['Nx'])
T = float(params['T'])
dt = float(params['dt'])
mesh = eval(params['mesh'])
V = VectorFunctionSpace(mesh, "Lagrange", degree)
u = Function(V)

viz_file = File("%s/u_solution.pvd" %path)

T = 0.85
dt = 0.005
i = 0
uval = 0
s = 0
u_array = []
for f in sorted(glob.glob("%s/u_solution_*.xml" %path)):
    print i
    ufile = File(f)
    ufile >> u
    tmp = u.vector().array()[10]
    u_array.append(tmp)
    # if tmp > 0:
    #     if tmp > uval:
    #         s += tmp
    #     else:
    #         s -= tmp
    # else:
    #     if abs(tmp) > uval
    #         s -= tmp
    #     else:
    #         s += tmp
    # uval = tmp
    i += 1
savetxt('uval_time1.txt', asarray(u_array))
