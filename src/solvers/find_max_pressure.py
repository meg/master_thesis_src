from numpy import *

def read_pressure_file(file_name):
    """
    Reads pressure data from file.
    Copyright Karen H. Stoeverud.

    :param file_name: The file to read from.
    :type file_name: str.
    """
    file_pres = open(file_name)
    lines_pres = file_pres.readlines()
    file_pres.close()
    len_file = len(lines_pres)

    start = 840 #- 170
    stop = 1010
    
    pres = zeros(len_file) 
    for i in range(len_file):
      pres[i] = float(lines_pres[i])
    pres_heartbeat = array(pres[start:stop])
    pres_heartbeat += 1.8287806 # make sure we have positive pressure values
    pres_heartbeat *= 1.3332 # convert from mmHg to Pa/100
    #print pres_heartbeat
    print len(pres_heartbeat)
    
    tt = linspace(0, (stop-start), num=(stop-start))
    tt = tt/200.#sampling frequence
    print len(tt)
    return tt, pres_heartbeat
p_file = "/home/nina/Dropbox/bitbucket/master_thesis/doc/txt/filtered_interval_1440000_to_1442000_WAVE_ICP-PAR_06072011_155906_16AE7DCB5-7C1_200.txt"

tt, p = read_pressure_file(p_file)

i = argmax(p)
print tt[i]
print tt