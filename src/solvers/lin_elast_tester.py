from LinearElasticityProblem import *
import numpy as np
from scipy.interpolate import splrep, splev

def compute(Nx, pval, degree, dim, dt=0.1, test=False, pressure=False, 
            makeplot=False, T=0.0, rectangle=False, cord=False,
            cordval=20, get_function=False, E=None, save_sol=False, 
            direct=False, simple=False, path=None, eigentester=False):

  # Adjust log level
  set_log_level(PROGRESS)

  # Turn on optimization
  parameters["form_compiler"]["cpp_optimize"] = True
  if test:
    parameters["krylov_solver"]["relative_tolerance"] = 1E-10
  
  Nt = int(round(T/dt))
  t = np.linspace(0, T, Nt+1)

  if simple:
    p = np.zeros(Nt+1)
    p[:Nt/2.] = pval

  # Set up mesh, cheching if mesh should be rectangular.
  mygeom = "square"
  if dim == 2:
    if rectangle:
      top = 3
      mesh = RectangleMesh(0, 0, 1, top, Nx, top*Nx)
    else:
      top = 1
      mesh = UnitSquareMesh(Nx,Nx)
      # mesh = Mesh("../meshes/tweaked_square.xml")
  elif dim == 3:
    if rectangle:
      top = 3
      nx = int(Nx/2.)
      mesh = BoxMesh(0, 0, 0, 1, 1, top, nx, nx, top*nx)
    elif cord:
      if cordval < 100:
        mygeom = "cord_simple"
        top = 2.99
        mesh = Mesh("../meshes/simplified_spinal_cord_%d.xml"%cordval)
        # mesh = Mesh("../meshes/marked_cord_points.xml")
      else:
        mygeom = "cord"
        top = 3.38757
        mesh = Mesh("/usit/abel/karenhes/Mesh/mesh_closedfissure_coarse.xml.gz")
      # plot(mesh, interactive=True)
    else:
      mygeom = "cube"
      top = 1
      mesh = UnitCubeMesh(Nx,Nx,Nx)
      # mesh = Mesh("../tweaked_cube.xml")

  # Set up function space, trial and test functions, facet normal
  V = VectorFunctionSpace(mesh, "Lagrange", degree)
  n = FacetNormal(mesh)

    # Set up the problem
  linElast = LinearElasticityProblem(degree, dim, V, n, dt, T, direct=True)

  #Set up test and trial functions etc.
  u = TrialFunction(V)
  v = TestFunction(V)
  linElast.set_trial_test_func(u, v)
  
  E1 = 0.84E3; E2 = 2.1E3;
  E = E1*E2/(E1+E2)
  linElast.set_parameters(E=E, nu=0.43)
  linElast.save_parameters(Nx, pval, degree, dim, dt, test, makeplot, T, 
                cordval, direct, simple)

  # Save parameter values to file if "save_sol" option is turned on
  if save_sol:
    linElast.set_save_file(path)
    params = linElast.params
    f = open("%s/params.txt" %path, 'w')
    for par in params.keys():
      f.write(par +" : " + str(params[par]) + "\n")
    f.close()
  
  # Set up boundaries and Dirichlet BCs
  linElast.set_bc(mesh, V, top=top, geom=mygeom)

  # Set up NO-ROTATION condition
  h = mesh.hmax()
  gamma = 2.0e3
  linElast.set_no_rotation(h, gamma, mygeom)

  # Set up body force:
  f = Constant([0.0]*dim)

  timestep=False
  v_wave = 200.0 # cm/s

  # Set up expressions for convergence testing if relevant
  if test==True:
    f, gval = linElast.test_convergence(p)

  #Set up expressions for pressure simulations if relevant
  elif pressure==True:
    if simple:
      if T > 0.1:
        timestep=True
        # gval = simple_pres()
        gval = Expression("p", p=float(p[0]))
    else:
      if T > 0.1:
        timestep = True
      file_name = "/home/nina/Dropbox/bitbucket/master_thesis/doc/txt/filtered_interval_1440000_to_1442000_WAVE_ICP-PAR_06072011_155906_16AE7DCB5-7C1_200.txt"
      tt, pres_heartbeat = linElast.read_pressure_file(file_name)
      # shift_val = 2.45458855332
      shift_val = 0.0
      pres_spline = splrep(tt, pres_heartbeat-shift_val)
      gval = linElast.applied_pres(v_wave, pres_spline, dim)

  linElast.set_g(gval)
  linElast.set_f(f)

  # Set up linear and bilinear forms
  linElast.form_a()
  # a, L = linElast.forms(u, v, f, gval, n, theta_proj, no_rotation, gamma, h)
  # Check eigenvalues of the matrix A, and save corresponding eigenvectors to
  # file if eigenvalue is (nearly) zero. This test will cancel the rest of
  # the simulation.
  # if eigentester:
  #   A = PETScMatrix()
  #   assemble(linElast.a, tensor=A, exterior_facet_domains=linElast.boundary_parts)
  #   linElast.calculate_vector_b()
  #   b = linElast.b
  #   for bc in linElast.bcs:
  #     bc.apply(A, b)
  #   eigensolver = SLEPcEigenSolver(A)
  #   print "Computing eigenvalues. This may take some time."
  #   eigensolver.solve()
  #   # # r, c, rx, cx =  eigensolver.get_eigenpair(0)
  #   # # print r
  #   num_eig = eigensolver.get_number_converged()
  #   print "Number of eigenvalues:", num_eig 

  #   tol = 1E-10
  #   for i in range(num_eig):
  #     r, c, rx, cx =  eigensolver.get_eigenpair(i)
  #     print "Eigenvalue %d: "%i, r
  #     if abs(r) < tol:
  #       eigv = Function(V)
  #       eigv.vector()[:] = rx
  #       eigf = File("eigenvec_%d.pvd" % i)
  #       eigf << eigv
  #   sys.exit(9)

  # Set up progress bar
  progress = Progress("Time-stepping...")

  # Set up array for values from strain tensor
  # V_ = TensorFunctionSpace(mesh, "Lagrange", degree)
  # eps = np.zeros(Nt+1)

  # Time loop (will only run once unless a pressure simulation is running)
  t = 0.0
  U = Function(V)
  u_array = np.zeros(Nt+1)
  # while t <= T + DOLFIN_EPS:
  for i in range(Nt+1):
    print "Step %d, t = %g" %(i, t)
    # Update the solution for the new time step
    U.assign(linElast.solver(i, matrix=True))

    # Save solution value from arbitrary point in geometry for plotting later
    # u_array[i] = U.vector().array()[58]
    # eps[i] = project(linElast.eps(U), V_).vector().array()[10]
    
    if makeplot==True:
      title="t=%g" %t
      plot(U, mode="displacement",interactive=True, title=title)
    
    if save_sol:
      linElast._saveFile(U, t)
    
    if timestep:
      progress.update(t/T)
      if simple:
        # gval.t = t + dt
        if i < Nt:
          gval.p = float(p[i+1])
        # print gval.get_val()
      else:
        gval.set_time(t + dt)

    # Update time step
    t += dt

  # Return the function U if relevant
  if get_function == True:
    return U

  # Calculate average and max displacements:
  # from scipy.integrate import simps, trapz
  # MPI.barrier()
  # area = simps(u_array, dx=dt)
  # print "Integral of random point over time: ", area
  # print "Average value: ", sum(u_array)/len(u_array)
  # print "Min val: ", np.min(np.abs(u_array))
  # import matplotlib.pyplot as plt
  # plt.plot(np.linspace(0, T, Nt+1), u_array)
  # plt.xlabel("t (s)")
  # plt.ylabel("u")
  # plt.show()

  # Calculate errornorm when running convergence test.
  if test==True:
    # Exact solution:
    u_e = linElast.exact_convergence(p)
    E = errornorm(u_e, U, "L2")
  else:
    E = 0
  MPI.barrier()
  print "Finished simulation."

  return  E


