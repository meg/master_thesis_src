from ElasticityProblem import *

problem = ElasticityProblem(1, 3, 0.005, 1.0)
problem.setup(10, cord=True, cordval=100)
problem.set_bc()
bp = problem.boundary_parts
u = File("boundary_parts.pvd")
u << bp