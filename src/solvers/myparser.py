import argparse

def my_parser():
    # Use an arg-parser to pick up optional and positional arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", help="Run simulation with pressure", 
                        action="store_true")
    parser.add_argument("--test", help="Run convergence test on solver", 
                        action="store_true")
    parser.add_argument("--time", help="Enter time to run simulation over", 
                        type=float, default=1.0)
    parser.add_argument("--res", help="Alter mesh resolution.", 
                        type=int, default=20)
    parser.add_argument("--gamma", help="gamma-value", type=float, default=2.0e3)
    parser.add_argument("--dt", help="Set dt-value", type=float, default=0.05)
    parser.add_argument("--save", help="Save results from simulation to file", 
                        action="store_true")
    parser.add_argument("--eig", help="Check eigenvectors of LHS matrix A. **NOTE**:\
                        This cancels the simulation.", action="store_true")
    parser.add_argument("--direct", help="Use direct LU solver. Default is Krylov solver.",
                        action="store_true")
    parser.add_argument("-p", help="Make plot", action="store_true")
    parser.add_argument("--dim", help="Choose dimension of problem (2D, 3D)", 
                        type=int, default=2)
    parser.add_argument("--simple", help="Use a simple step pressure.", 
                        action="store_true")
    parser.add_argument("-r", help="Use a rectangle geometry", action="store_true")
    parser.add_argument("-c", help="Use a cord geometry", action="store_true")
    parser.add_argument("-v", help="Run simulation with viscoelasticity model", 
                        action="store_true")
    parser.add_argument("pval", type=float, help="Value of p")
    parser.add_argument("degree", type=int, help="Value of degree to use")
    return parser.parse_args()
