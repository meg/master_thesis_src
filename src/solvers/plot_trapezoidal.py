from matplotlib.pyplot import *
from numpy import *

t = linspace(0.2, 2*pi - 0.2, 1001)
s = sin(t)
xtick = np.asarray([0.2, 2*pi - 0.2])

plot(t,s)
frame1 = gca()
# frame1.axes.get_xaxis().set_visible(False)
frame1.axes.get_yaxis().set_visible(False)
xticks(xtick, ['a', 'b'])
axis([0.0, 2*pi, -2, 2])
show()