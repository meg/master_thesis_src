# from LinearElasticityProblem import *
import lin_elast_tester as linElast
# import viscotester as viscoElast
import compute_solutions as comp
import test_convergence
from myparser import my_parser
from time import gmtime, strftime, localtime
import os, sys
 
def convergence(compute,p,degree,dim,direct,cube=False):
    h = []  # element sizes
    E = []  # errors
    if cube:
        nlist = [2, 4, 8, 16, 20]
    else:
        nlist = [4, 8, 16, 32, 64, 128]
    for N in nlist:
        h.append(1.0/N)
        E.append(compute('linElast', N, p, degree, dim, direct=direct))

    print "Finished computing h and E."

    # Convergence rates
    from math import log as ln #(log is a dolfin name too)
    print "Convergence rate:"
    print "h=%10.2E  E %2.6E" % (h[0], E[0])
    for i in range(1, len(E)):
        r = ln(E[i]/E[i-1])/ln(h[i]/h[i-1])
        print "h=%10.2E  E %2.6E r=%.2f" % (h[i], E[i], r)
# MPI.barrier()

args = my_parser()
print args
print type(args)
print sys.argv
# sys.exit(0)
p = args.pval
degree = args.degree

pres_file = "/home/ninal/Downloads/filtered_interval_1440000_to_1442000_WAVE_ICP-PAR_06072011_155906_16AE7DCB5-7C1_200.txt"

def make_save_path():
    import re
    t = localtime()
    t_str = strftime("%b%d_%H:%M_", t)

    for a in sys.argv[1:]:
      m = re.match("-([a-z]*)", a)
      if m is not None:
        t_str += m.group(1)

    path = 'results/' + t_str
    os.system("mkdir %s" % path)
    return path

# Create path to save results
path = None
if args.save:
  path = make_save_path()

# Turn plotting on/off
if args.p:
  makeplot=True
  print "makeplot=True"
else:
  makeplot=False
  print "makeplot=False"

if args.r:
  rectangle = True
else:
  rectangle = False

if args.c:
  cord = True
  args.dim = 3
  if (args.res) is None:
    args.res=20
else:
  cord = False

if args.test:
  test = True
  makeplot = False
else:
  test = False

if args.time:
  T = args.time
else:
  T = 0.0

# MPI.barrier()
if args.s:
  Nx = int(args.res)
  if args.v:
    prob = 'viscoElast'
    # viscoElast.compute(Nx, args.pval, args.degree, args.dim, dt=args.dt,
    #     makeplot=makeplot, T=args.time, rectangle=rectangle, cord=cord, 
    #     cordval=args.res, save_sol=args.save, direct=args.direct, 
    #     simple=args.simple, path=path)
  else:
    prob = 'linElast'
    # linElast.compute(Nx, args.pval, args.degree, args.dim, dt=args.dt, pressure=True, 
    #   makeplot=makeplot, T=args.time, rectangle=rectangle, cord=cord, 
    #   cordval=args.res, save_sol=args.save, direct=args.direct, 
    #   simple=args.simple, path=path, eigentester=args.eig)
  
  comp.compute(prob, Nx, args.pval, args.degree, args.dim, pres_file, dt=args.dt,
      makeplot=makeplot, T=args.time, rectangle=rectangle, cord=cord, 
      cordval=args.res, save_sol=args.save, direct=args.direct, 
      simple=args.simple, path=path, eigentester=args.eig, gamma=args.gamma)
elif args.test:
  cube = False
  if args.dim == 3:
    cube = True
  convergence(test_convergence.compute, args.pval, args.degree, args.dim,
            direct=args.direct, cube=cube)
