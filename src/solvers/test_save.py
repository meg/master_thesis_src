from dolfin import *
from numpy import savetxt, asarray
import glob, sys

try:
    path = sys.argv[1]
except IndexError:
    path = "/home/nina/Dropbox/bitbucket/master_thesis_src/src/solvers/results/May09_13:22_svc"
# path = "/home/nina/Dropbox/bitbucket/master_thesis/src/results_abel/May08_14:58_scv"
f = open("%s/params.txt" %path)
lines = f.readlines()
params = {}
for line in lines:
    words = line.split(' : ')
    val = words[1].rstrip()
    params[words[0]] = val

print params
f.close()

print "Setting parameters"
degree = int(params['degree'])
Nx = int(params['Nx'])
T = float(params['T'])
dt = float(params['dt'])
print "reading mesh..."
mesh = eval(params['mesh'])
print "done"
V = VectorFunctionSpace(mesh, "Lagrange", degree)
u = Function(V)

viz_file = File("%s/u_solution.pvd" %path)

T = 0.85
dt = 0.005
i = 0
uvals = []
for f in sorted(glob.glob("%s/u_solution_*.xml" %path)):
    print i
    if i%1 == 0:
        ufile = File(f)
        ufile >> u
        uvals.append(u.vector().array()[10])
        viz_file << (u, i*dt)
    i += 1

savetxt('%s/u_array_time.txt' %path, asarray(uvals))
