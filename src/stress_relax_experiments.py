from sympy.functions.special.delta_functions import Heaviside, DiracDelta
from numpy import exp, linspace, zeros, asarray, savetxt, ones
import matplotlib.pyplot as plt

def Maxwell_stress(t, t0, Nt):
    
    E = 10.0
    eta = 2.0
    eps_0 = 0.2
    sigma = zeros(Nt)
    for i in range(Nt):
        sigma[i] = E*exp(-E/eta*(t[i]-t[t0]))*eps_0*Heaviside(t[i] - t[t0])
    print E*eps_0
    return sigma 

def kv_stress(t, t0, Nt):
    E = 10.0
    eta = 2.0
    eps_0 = 0.2
    sigma = zeros(Nt)
    for i in range(Nt):
        sigma[i] = E*eps_0*Heaviside(t[i]-t[t0]) + eta*eps_0*DiracDelta(t[i]-t[t0])
    return sigma

def sls_stress(t, t0, Nt):
    E1 = 10.0
    E2 = 10.0
    eta = 2.0
    eps_0 = 0.2
    tau_e = eta/E2
    tau_s = eta*((E1+E2)/(E1*E2))
    print E1*eps_0*tau_s/tau_e
    print E1*eps_0
    # sigma = zeros(Nt)
    # for i in range(Nt):
    #     sigma[i] = (E1 + E2*exp(-(t[i]-t[t0])/tau_e))*eps_0*Heaviside(t[i]-t[t0])
    # return sigma

t = linspace(0, 1, 1001)
Nt = len(t)
t0 = int(Nt/6.)

# eps_0 = zeros(Nt)
# eps_0[t0+1:] = 0.2
# savetxt("step_strain.txt", eps_0)
# print "OK"

sigma = sls_stress(t, t0, Nt)
print 4/(2./3)
# savetxt("sls_stress.txt", sigma)
print "OK"




# plt.plot(t, sigma)
# xticks = asarray([t[t0]])
# yticks = asarray([0, sigma[t0+1]])
# plt.xticks(xticks, ['$t_0$'])
# plt.yticks(yticks, [0,r'$E\varepsilon_0$'])
# plt.ylabel(r'$\sigma$')
# plt.xlabel('t')
# plt.show()

# print Maxwell_stress(t)
# plot(t, sigma)
# plt.hold('on')
# plt.vlines(x=t[t0], ymin=0.0, ymax=sigma[t0+1], color='b')
# xticks = np.asarray([t[t0]])
# yticks = np.asarray([0, sigma[t0+1]])
# plt.xticks(xticks, ['$t_0$'])
# plt.yticks(yticks, [0,r'$E\varepsilon_0$'])
# plt.ylabel(r'$\sigma$')
# plt.xlabel('t')

# plt.show()
