""" Implement the trapezoidal rule and test it for a given function """
"""
    def trapezoidal(self, i):
        Ds, u = self.Ds, self.u
        t, dt = self.t, self.dt
        eps = self.eps
        s = 0*eps(self.u[0])
        for j in range(1, i):
            s += Ds(t[i]-t[j])*eps(u[j]) + Ds(t[i]-t[j-1])*eps(u[j-1])
        return 0.5*dt*s
"""
import numpy as np
import math as m
import matplotlib.pyplot as plt

class Problem:
  def exact(self, t):
    return 0.5*(np.exp(-t) + np.sin(t) - np.cos(t))

  def Ds(self, t):
    return m.exp(-(t))

  def eps(self, u):
    return m.sin(u)

class Trap_Tester(Problem):

  def __init__(self, a, b, n):
    self.t = np.linspace(a, b, n)
    self.dt = self.t[1] - self.t[0]

  def sum(self, i):
    t, dt = self.t, self.dt
    Ds, eps = self.Ds, self.eps
    s = 0.5*Ds(t[i] - t[0])*eps(t[0])
    for j in range(1, i):
      s += Ds(t[i] - t[j])*eps(t[j])
    return dt*s

  def end_term(self, i):
    t, dt = self.t, self.dt
    Ds, eps = self.Ds, self.eps
    return 0.5*dt*Ds(0)*eps(t[i])

class Efficient_Trap_Tester(Problem):

  def __init__(self, a, b, n):
    self.t = np.linspace(a, b, n)
    self. dt = self.t[1] - self.t[0]
    self.tot_sum = 0
    self.temp_sum = 0

  def sum(self, i):
    t, dt = self.t, self.dt
    Ds, eps = self.Ds, self.eps
    if i == 0:
      self.temp_sum = Ds(t[i]-t[0])*eps(t[0])
      self.tot_sum = 0.5*dt*self.temp_sum
    else:
      self.temp_sum = np.exp(-(t[i]-t[i-1]))*self.temp_sum \
                      + Ds(t[i]-t[i-1])*eps(t[i-1])
      self.tot_sum = dt*self.temp_sum
    return self.tot_sum

  # def efficient_sum_first(self, i):
  #   t, dt = self.t, self.dt
  #   Ds, eps = self.Ds, self.eps
    
  #   if i == 0:
      
  #     return self.tot_sum

  def end_term(self, i):
    t, dt = self.t, self.dt
    Ds, eps = self.Ds, self.eps
    return 0.5*dt*Ds(0)*eps(t[i])

def compute(n):
  a = 0; b = 1; 
  test = Trap_Tester(a, b, n)
  t = test.t
  dt = test.dt
  sol = np.zeros(n)
  # trapn = 0.5*dt*test.Ds(0)*test.eps(t[-1])
  sol[0] = test.trapezoidal(0)
  for i in range(1,n):
    sol[i] = test.trapn(i) + test.trapezoidal(i)
  error = test.exact(t) - sol
  E = m.sqrt(dt*sum(error**2))
  return E

def convergence(compute):
    h = []  # dt
    E = []  # errors
    # dt_list = [0.08, 0.04, 0.02, 0.01, 0.005, 0.0025]
    nlist = [11, 21, 41, 81, 161, 321, 641, 1281]
    N = 8
    for n in nlist:
        h.append(1./n)
        Eval = compute(n)
        E.append(Eval)


    print "Finished computing h and E."
    print E

    # Convergence rates
    from math import log as ln #(log is a dolfin name too)
    print "Convergence rate:"
    for i in range(1, len(E)):
        r = ln(E[i]/E[i-1])/ln(h[i]/h[i-1])
        print "h=%10.2E r=%.2f" % (h[i], r)


if __name__ == '__main__':
  convergence(compute)
  # n = 11
  # t, sol, exact = compute(n)
  # plt.figure()
  # plt.plot(t, sol, t, exact)
  # plt.show()